package tran;

import main.Main;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;
import tran.homework.HomeworkHandeller;

public class Tran {
	PApplet p;
	int yPos = Math.round(Main.gridSize.y/2);
	PVector pos, size;
	int pMillis;
	
	public Tran(PApplet p) {
		this.p = p;
		size = new PVector(p.width/Main.gridSize.x, p.height/Main.gridSize.y);
		pos = new PVector(p.width-size.x, 0);
	}
	
	public void display(){
		PImage tran = p.loadImage("tran/tran.png");
		p.image(tran, pos.x, pos.y, size.x, size.y);
	}
	
	public void run(){
		if(p.millis() - pMillis >= 500){
			pMillis = p.millis();
//			int movement;
//			if(yPos < Math.round(Main.gridSize.y))
//				movement = Math.round(p.random(-5, 1));
//			else if(yPos > Math.round(Main.gridSize.y))
//				movement = Math.round(p.random(-1, 5));
//			else 
//				movement = Math.round(p.random(-1, 1));
//
//			if(movement >= 0)
//				yPos += 1;
//			else
//				yPos -= 1;
			yPos = Math.round(Main.gridSize.y*p.noise(p.millis()/1000));
			putYposInBounds();
			calcPos();

			int shoot = Math.round(p.random(0, 5));
			if(shoot >= 1)
				HomeworkHandeller.createHomework(p, pos.copy().add(-size.x/2, size.y/2));
		}
	}
	
	private void putYposInBounds(){
		yPos = Math.max(0, yPos); //if yPos < 0, yPos = 0
		yPos = Math.min(Math.round(Main.gridSize.y-1), yPos); //if yPos > gridSize, yPos = gridSize
	}
	private void calcPos(){
		pos.y = yPos*size.y;
	}
}
