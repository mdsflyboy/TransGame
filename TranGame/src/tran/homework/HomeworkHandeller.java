package tran.homework;

import java.util.ArrayList;
import java.util.Timer;

import main.Main;
import player.Player;
import processing.core.PApplet;
import processing.core.PVector;

public class HomeworkHandeller {
	public static ArrayList<Homework> homeworks = new ArrayList<>();
	static boolean end = false;
	static Player p;

	public static void createHomework(PApplet p, PVector pos) {
		Homework hw = new Homework(p, pos);
		homeworks.add(hw);
	}

	public static void setPlayer(Player _p) {
		p = _p;
	}


	public static void end() {
		end = true;
	}
	public static Thread Handeller = new Thread() {
		@Override
		public void run() {
			while (!end) {
				for (int i = 0; i < homeworks.size(); i++) {
					homeworks.get(i).run();
					homeworks.get(i).display();
					if (homeworks.get(i).pos.x < 0) {
						Main.missed += Homework.getValue();
						homeworks.remove(i);
					} else if (homeworks.get(i).hitPlayer(p)) {
						Main.caught += Homework.getValue();
						homeworks.remove(i);
					}
				}
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	};
}
