package tran.homework;

import main.Main;
import player.Player;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

public class Homework {
	PApplet p;
	PVector pos;

	private static PApplet staticP;
	public static float speed; // pixels/sec
	public static PVector size;
	
	public Homework(PApplet p, PVector pos) {
		staticP = p;
		this.p = p;
		speed = p.width/2;
		this.pos = pos;
		size = new PVector(p.width/Main.gridSize.x, p.height/Main.gridSize.y);
		size.mult((float) 0.5);
	}
	
	
	public void run(){
		pos.x -= speed/p.frameRate;
	}

	public void display(){
		PImage img = p.loadImage("tran/homework/hw.png");
		p.image(img, pos.x-size.x/2, pos.y-size.y/2, size.x, size.y);
	}
	
	public PVector gridPos(){
		PVector gridPos = new PVector();
		float gridSquareSzX = p.width/Main.gridSize.x;
		float gridSquareSzY = p.height/Main.gridSize.y;
		gridPos.x = (float) Math.floor(pos.x/gridSquareSzX);
		gridPos.y = (float) Math.floor(pos.y/gridSquareSzY);
		return gridPos;
	}
	public boolean hitPlayer(Player p){
		if (gridPos().x == 0 && gridPos().y == p.getYPos()) {
			return true;
		}else{
			return false;
		}
	}
	
	public static float getValue(){
		try{
			return (float) Math.pow(1.5, staticP.millis()/1000);
		}catch(NullPointerException e){
			return 1;
		}
	}
}
