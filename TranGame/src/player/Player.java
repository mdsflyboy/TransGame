package player;

import main.Main;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;

public class Player {
	PApplet p;
	PVector pos, size;
	int yPos = 0;
	
	public Player(PApplet p) {
		this.p = p;
		pos = new PVector();
		size = new PVector(p.width/Main.gridSize.x, p.height/Main.gridSize.y);
		yPos = Math.round(p.random(Main.gridSize.y));
		putYposInBounds();
		calcPos();
	}
	
	public int getYPos(){
		return yPos;
	}
	public void run(){
//		if(p.keyPressed){
//			switch(p.keyCode){
//			case PApplet.UP:
//				moveUp();
//				break;
//			case PApplet.DOWN:
//				moveDown();
//				break;
//			default:
//				break;
//			}
//		}
		calcPos();
	}

	public void display(){
		PImage player = p.loadImage("player/player.png");
		p.image(player, pos.x, pos.y, size.x, size.y);
	}
	
	public void moveUp(){
		yPos--;
		putYposInBounds();
	}
	
	public void moveDown(){
		yPos++;
		putYposInBounds();
	}
	
	private void putYposInBounds(){
		yPos = Math.max(0, yPos); //if yPos < 0, yPos = 0
		yPos = Math.min(Math.round(Main.gridSize.y-1), yPos); //if yPos > gridSize, yPos = gridSize
	}
	private void calcPos(){
		pos.y = yPos*size.y;
	}
}
