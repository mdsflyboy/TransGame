package main;

import player.Player;
import processing.core.PApplet;
import processing.core.PImage;
import processing.core.PVector;
import tran.Tran;
import tran.homework.Homework;
import tran.homework.HomeworkHandeller;

public class Main extends PApplet {
	public static void main(String[] args) {
		PApplet.main("main.Main");
	}

	public static PVector gridSize = new PVector(10, 5);
	public static int caught = 1, missed = 0;

	public static enum GameStates {
		start, running, pause, over;
	}

	GameStates state = GameStates.start;

	public void settings() {
//		size(500, 500);
		fullScreen();
	}

	Tran tran;
	Player player;
	PImage background;
	double cPercent = 100;
	double finalTime = 0;

	public void setup() {
		frameRate(120);
		tran = new Tran(this);
		player = new Player(this);
		
		background = loadImage("main/background.jpg");
	}

	public void draw() {
		switch (state) {
		case start:
			background(0, 150, 150);
			textAlign(PApplet.CENTER, PApplet.CENTER);
			fill(60,0,0);
			int sections = 10;
			textSize(height/(2*sections));
			text("Homework Runner!", 0,0,width,height/sections);
			textAlign(PApplet.LEFT, PApplet.CENTER);
			int num = 1;
			text("    Goal: DO every one of Ms.Tran's homwork assignments!",0,num * height/sections,width,(num+1)*height/sections);
			num++;
			text("    Every second, the value of each assignment increases!",0,num*height/sections,width,(num+1)*height/sections);
			num++;
			text("    If your grade becomes an F (60%), YOU LOSE!",0,num*height/sections,width,(num+1)*height/sections);
			num++;
			text("    Use Arrow Keys to control your character.",0,num*height/sections,width,(num+1)*height/sections);
			num++;
			textAlign(PApplet.CENTER, PApplet.CENTER);
			text("Press any button to continue...",0,num*height/sections,width,(num+1)*height/sections);
			num++;
			HomeworkHandeller.setPlayer(player);
			break;
		case running:
			background(160);
			image(background, 0, 30, width, height-30);
			tran.run();
			tran.display();
			player.run();
			player.display();
			textAlign(PApplet.CENTER, PApplet.CENTER);
			textSize(15);
			cPercent = caught + missed != 0 ? Math.round(100 * caught / (caught + missed)) : 0;
			fill(255);
			text("Grade: " + cPercent + "%" + "    Current Value: " + Homework.getValue() + "   Time: "+millis()/1000, 0, 0, width, 30);
			if (cPercent < 60){
				finalTime = millis()/1000;
				state = GameStates.over;
			}
			break;
		case pause:
			break;
		case over:
			HomeworkHandeller.end();
			background(0);
			textAlign(PApplet.CENTER, PApplet.CENTER);
			textSize(50);
			fill(255, 0, 0);
			text("GAME OVER \nTime Lasted: "+finalTime+" seconds \nPress Esc to exit", 0, 0, width, height);
			break;
		}
	}

	public void keyPressed() {
		if (state == GameStates.start) {
			HomeworkHandeller.Handeller.start();
			state = GameStates.running;
		} else {
			switch (keyCode) {
			case PApplet.UP:
				player.moveUp();
				break;
			case PApplet.DOWN:
				player.moveDown();
				break;
			default:
				if (key == 'p') {
					if (state != GameStates.pause)
						state = GameStates.pause;
					else
						state = GameStates.running;
				}
				break;
			}
		}
	}
}
